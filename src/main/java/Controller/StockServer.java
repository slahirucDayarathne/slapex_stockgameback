package Controller;

import Model.Broker;
import Model.Player;
import akka.NotUsed;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.http.javadsl.ConnectHttp;
import akka.http.javadsl.Http;
import akka.http.javadsl.model.HttpRequest;
import akka.http.javadsl.model.HttpResponse;
import akka.http.javadsl.server.AllDirectives;
import akka.http.javadsl.server.Route;
import akka.stream.ActorMaterializer;
import akka.stream.javadsl.Flow;

public class StockServer extends AllDirectives {

    private final GameRoutes gameRoutes;

    public StockServer(ActorSystem system , ActorRef playerActor , ActorRef broker){
        gameRoutes = new GameRoutes(system, playerActor , broker);
    }

    public static void main(String[] args) {

        ActorSystem system = ActorSystem.create("StockActorSystem");

        final Http http = Http.get(system);
        final ActorMaterializer materializer = ActorMaterializer.create(system);

        ActorRef playerActor = system.actorOf(Props.create(Player.class), "player");
        ActorRef brokerActorRef = system.actorOf(Props.create(Broker.class), "broker");

        System.out.println("Server is attempting to START");
        StockServer app = new StockServer(system, playerActor,brokerActorRef);
        final Flow<HttpRequest, HttpResponse, NotUsed> routeFlow = app.createRoute().flow(system, materializer);
        http.bindAndHandle(routeFlow, ConnectHttp.toHost("localhost", 8087), materializer);
        System.out.println("Server STARTED");
    }

    protected Route createRoute() {
        return gameRoutes.routes();
    }
}
