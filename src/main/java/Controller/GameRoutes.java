package Controller;

import Model.Broker;
import Model.Player;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.http.javadsl.marshallers.jackson.Jackson;
import akka.http.javadsl.model.StatusCodes;
import akka.http.javadsl.server.AllDirectives;
import akka.http.javadsl.server.PathMatchers;
import akka.http.javadsl.server.Route;
import akka.pattern.Patterns;
import com.google.gson.Gson;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.CompletionStage;

public class GameRoutes extends AllDirectives {

    String s = "Lahiru Chandima";
    Gson g ;

    final private ActorRef playerActor;
    final private LoggingAdapter log;
    final private ActorRef brokerActor;

    public GameRoutes(ActorSystem system , ActorRef playerActor , ActorRef brokerActor){
        this.playerActor = playerActor;
        this.brokerActor = brokerActor;
        log = Logging.getLogger(system,this);
    }

    Duration timeout = Duration.ofSeconds(51);

    public Route routes() {
        System.out.println("Here Achieved");
        return route(pathPrefix("player", () ->
                route(
                        addGamePlayer(),
                        path(PathMatchers.segment(),name->route(
                                getShares(name),
                                updateShares(name)
                        ))
                )
        ));

    }

    private Route updateShares(String name) {
        return

                get(() -> {
                    CompletionStage<Broker.AllGAmeDataMessage> futureUsers = Patterns
                            .ask(brokerActor, new Broker.AllGAmeDataMessage(Broker.returnPlayerList(),Broker.returnPlayGameList(),Broker.returnEventImpactList()) , timeout)
                            .thenApply(Broker.AllGAmeDataMessage.class::cast);
                    return onSuccess(() -> futureUsers,
                            performed -> complete(StatusCodes.OK, performed, Jackson.marshaller())

                    );
                });
        //#users-delete-logic
    }

    private Route getShares(String name){
        System.out.println("Post Play");
        return post( () ->
                entity(
                        Jackson.unmarshaller(Broker.PlayerGame.class),
                        data -> {
                            System.out.println("Player data is going to create");
                            System.out.println("Priceee :" +data.shareAmounts[1]);
                            CompletionStage<Broker.PlayerGame> create = Patterns
                                    .ask(brokerActor , data ,timeout)
                                    .thenApply(Broker.PlayerGame.class::cast);
                            return onSuccess(() -> create,
                                    performed -> {
                                        log.info("Created user data [{}]: {}", "d", "s");
                                        return complete(StatusCodes.CREATED, performed.shareAmounts, Jackson.marshaller());
                                    });
                        }
                ));
    }

    private Route addGamePlayer(){
        System.out.println("Here achieved 2");
        return pathEnd(() ->
                route(
                        get(() -> {
                            CompletionStage<Player.Getdata> futureUsers = Patterns
                                    .ask(playerActor, new Player.Getdata("La"), timeout)
                                    .thenApply(Player.Getdata.class::cast);
                            return onSuccess(() -> futureUsers,
                                    performed -> complete(StatusCodes.OK, performed.returnName(), Jackson.marshaller())

                            );
                        }),
                        post( () ->
                                entity(
                                        Jackson.unmarshaller(Broker.Player1.class),
                                        player -> {
                                            System.out.println("Player is going to create");
                                            System.out.println(player.getClass());
                                            System.out.println("player Name :" + player.name);
                                            CompletionStage<Broker.InitiatingMessage> create = Patterns
                                                    .ask(brokerActor , new Broker.InitiatingMessage(player.name,player.cashBalance),timeout)
                                                    .thenApply(Broker.InitiatingMessage.class::cast);
                                            return onSuccess(() -> create,
                                                    performed -> {
                                                        log.info("Created user [{}]: {}", "d", "s");
                                                        List plaList = performed.returnList();
                                                        for (int a = 0 ; a < plaList.size() ; a++){
                                                            Broker.Player1 p = (Broker.Player1) plaList.get(a);
                                                            System.out.println("My name is :" + p.name);
                                                            System.out.println("My balance is :" + p.cashBalance);
                                                        }
                                                        return complete(StatusCodes.CREATED, performed.returnList(), Jackson.marshaller());
                                                    });
                                        }
                                ))

                ));

    }




}
