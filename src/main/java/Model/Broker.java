package Model;

import akka.actor.AbstractActor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Broker extends AbstractActor implements Serializable{

    public Broker(){
        addDoubleToList();
    }

    public static class Player1 {
        public String name;
        public String cashBalance;

    }

    public static class PlayerGame {

        public double[] sharePrices;
        public String[] shareAmounts;


    }

    public static class AllGAmeDataMessage implements Serializable{
        public List<Player1> topPlayer ;
        public List<PlayerGame> playerGame;
        public List<Double> evenDoubleList;

        public AllGAmeDataMessage(List l1 , List l2 , List l3){
            topPlayer = l1;
            playerGame = l2;
            evenDoubleList = l3;
        }
    }

    public static void addDoubleToList(){
//        for (int a = 0 ; a < 5 ; a++){
            GameAlgo ga = new GameAlgo(5);
            eventImpactList.add(ga.stock[0]);
            eventImpactList.add(ga.stock[1]);
            eventImpactList.add(ga.stock[2]);
            eventImpactList.add(ga.stock[3]);
            eventImpactList.add(ga.stock[4]);
//        }
    }


        static public List<Player1> playerList = new ArrayList<>();
        static public List<PlayerGame> playGameList = new ArrayList<>();
        static public List<Double> eventImpactList = new ArrayList<>();

        static public List<Player1> returnPlayerList(){
            return playerList;
        }
    static public List<PlayerGame> returnPlayGameList(){
        return playGameList;
    }
    static public List<Double> returnEventImpactList(){
            return eventImpactList;
    }

    static public class InitiatingMessage implements Serializable{
        Player1 pp ;
        public InitiatingMessage(){

        }
        public InitiatingMessage(String s1 , String s2){
            System.out.println("Constructor executed    ");
            pp = new Player1();
            pp.name = s1;
            pp.cashBalance = s2;
        }

        public List<Player1> returnList(){
            return playerList;
        }
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(Broker.InitiatingMessage.class, this::addUserToTheSystem)
                .match(Broker.PlayerGame.class, this::gameData)
                .match(Broker.AllGAmeDataMessage.class, this::sendGameData)
                .build();
    }
    public void addUserToTheSystem(Broker.InitiatingMessage d){
        System.out.println("HereAlso Came");
        playerList.add(d.pp);
        System.out.println(playerList);
        getSender().tell(new Broker.InitiatingMessage() , getSelf());
    }

    public void gameData(Broker.PlayerGame p){
        System.out.println("Player game data is storing");
        playGameList.add(p);
        getSender().tell(p, getSelf());
    }

    public void sendGameData(Broker.AllGAmeDataMessage d){
        System.out.println("All game data going to release");
        getSender().tell(new Broker.AllGAmeDataMessage(d.topPlayer, d.playerGame,d.evenDoubleList) , getSelf());
    }
}
