package Model;

import java.util.Arrays;
import java.util.Random;

public class GameAlgo {



    public int x1 = 0;
    public int x2[] =new int[10];
    public int x5[][]=new int[4][10];

    double[] arry;

    public GameAlgo(int no){
        x1 = calculateRandomTrend();
        x2 = calculateMarketTrend();
        x5 = generateSectorTrendArray();
        arry=startTurn(no);
        System.out.println(Arrays.toString(arry));

    }


    public int calculateRandomTrend(){
        int randomTrendArray[]={-2, -1, 0, 1, 2};
        Random random1=new Random();
        int x1=randomTrendArray[random1.nextInt(5)];


        return x1;
    }


    public int[] calculateMarketTrend()
    {

        int marketTrendArray[]=new int [10];
        int randomArray[]={0,1,-1};
        int sum=0;
        for(int i=0; i<10; i++)
        {
            Random random2=new Random();

            int x2=random2.nextInt(3);
            if(sum<3 && sum>-3) {
                sum=sum+randomArray[x2];
                marketTrendArray[i] = randomArray[x2];

            }
            else{
                marketTrendArray[i]=0;
            }
        }

        return marketTrendArray;
    }


    public int[] calculateSectorTrend()
    {
        //calculate market trend
        int sectorTrendArray[]=new int [10];
        int randomArray2[]={0,1,-1};
        int sum2=0;
        for(int i=0; i<10; i++)
        {
            Random random3=new Random();

            int x3=random3.nextInt(3);
            if(sum2<3 && sum2>-3) {
                sum2=sum2+randomArray2[x3];
                sectorTrendArray[i] = randomArray2[x3];

            }
            else{
                sectorTrendArray[i]=0;
            }
        }

        return sectorTrendArray;
    }

    public int[][]  generateSectorTrendArray() {

        int sectorTrendValueArray[][] = new int[5][10];


        sectorTrendValueArray[0] = calculateSectorTrend();
        sectorTrendValueArray[1] = calculateSectorTrend();
        sectorTrendValueArray[2] = calculateSectorTrend();
        sectorTrendValueArray[3] = calculateSectorTrend();
        sectorTrendValueArray[4] = calculateSectorTrend();


        return sectorTrendValueArray;
    }

    public  int[] matchSectorTrend( int turnNo, int Array[][]){
        //String sector = secName;
        int turn = turnNo;
        int Array1[][]=Array;
        String sectorNameArray[] = {"technolog", "education", "tourism", "agriculture", "transport"};
        //int sectorTrendValue = 0;
        int[] sectorTrendValue=new int[5];//new
        for (int i = 0; i < sectorNameArray.length; i++) {


            sectorTrendValue[i]=Array1[i][turn - 1];
        }
        return sectorTrendValue;
    }



    public int calculateEventTrend(int turnNo){

        int turn=turnNo;

        Random random4=new Random();
        String x4;//eventArray[random4.nextInt(5)];
        int prob=random4.nextInt(100)+1;

        int value=0;

        String sectorEvent[]={"boom","bust"};//both have 0.5 probability in selection
        String stockEvent[]={"profit_warning","profit_warning","scandal","take_over"};
        //profit warning has gained 0.5 and others 0.25 of probabilities

        if(turn>=2 && turn<=5) {
            if (prob <= 34) {

                int sec = random4.nextInt(2);
                x4 = sectorEvent[sec];

                if (x4 == "boom") {
                    System.out.println("boom");
                    int boomArray[] = {1, 2, 3, 4, 5};
                    value = boomArray[new Random().nextInt(5)];
                } else {
                    System.out.println("bust");
                    int bustArray[] = {-1, -2, -3, -4, -5};
                    value = bustArray[new Random().nextInt(5)];
                }
            } else {
                int stock = random4.nextInt(4);
                x4 = stockEvent[stock];

                if (x4 == "profit_warning") {
                    System.out.println("profit_w");
                    int pwArray[] = {-1, -2, -3, -4, -5};
                    value = pwArray[new Random().nextInt(5)];
                } else if (x4 == "scandal") {
                    System.out.println("scan");
                    int scandalArray[] = {-3, -4, -5, -6};
                    value = scandalArray[new Random().nextInt(4)];
                } else {
                    System.out.println("take_o");
                    int takeoverArray[] ={2,3} ;
                    value = takeoverArray[new Random().nextInt(2)];
                }
            }
        }
        else if(turn<=7)
        {
            int stock1=random4.nextInt(4);
            x4=stockEvent[stock1];

            if (x4=="profit_warning"){
                System.out.println("profit_warning");
                int pwArray[]={-1,-2,-3,-4,-5};
                value=pwArray[new Random().nextInt(5)];
            }
            else if (x4=="scandal"){
                System.out.println("scandal");
                int scandalArray[]={-3,-4,-5,-6};
                value=scandalArray[new Random().nextInt(4)];
            }
            else {
                System.out.println("take_over");
                int takeoverArray[]={2,3};
                value=takeoverArray[new Random().nextInt(2)];
            } }
        else{
            System.out.println("no event");
            value=0;
        }
        return value;
    }

    double []stock=new double[5];
    public double[] startTurn(int tNo){

        stock=new double[5];

        int turn1 = tNo;//1

        int x3 = x2[turn1 - 1];
        int x4 = calculateEventTrend(turn1);
        int x8[] =matchSectorTrend(turn1,x5);

        for(int j=0;j<5;j++) {
            if ((x1 + x3 + x4 + x8[j]) > 0) {
                stock[j] = x1 + x3 + x4 + x8[j];

            }
            else {
                stock[j] = 0;
            }
        }
        return stock;
    }

    public static void main(String[] args) {
//        GameAlgo ga=new GameAlgo(18);
//        System.out.println(ga.stock[1]);
//
        Random rnd = new Random();
        for (int a = 0 ; a < 10 ; a++){
            System.out.println(rnd.nextInt(4));
        }
    }

}
