package Model;

import akka.actor.AbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;

import java.io.Serializable;

public class Player extends AbstractActor implements Serializable {

    LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);

    static public class Getdata implements Serializable {
        String name ;
            public Getdata(String name){
                this.name = name;
            }
            public String returnName(){
                return name;
            }
    }

    public String name;
    public String cashBalance;

    public Player(){}

    public Player(String name , String cashBalance){
        this.name = name;
        this.cashBalance = cashBalance;
    }

    @Override
    public Receive createReceive() {
        System.out.println("Here3");
        return receiveBuilder()
                .match(Player.class, this::returnName)
                .match(Player.Getdata.class, this::returnName2)
                .matchAny(o -> log.info("received unknown message")).build();
    }

    public void returnName(Player p){
        System.out.println("Hello There");
//        getSender().tell(new ,getSelf());
    }
    public String returnName2(Player.Getdata p){
        System.out.println("Hello There tooo");
        getSender().tell(new Player.Getdata("Lahiru Chandima"),getSelf());
        return  "Lahiru2";
    }
}
