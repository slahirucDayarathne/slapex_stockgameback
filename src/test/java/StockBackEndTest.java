import Model.Broker;
import Model.Player;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.testkit.javadsl.TestKit;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class StockBackEndTest {

    static ActorSystem system;

    @BeforeClass
    public static void setup(){
        system = ActorSystem.create();
    }

    @AfterClass
    public static void tearDown(){
        TestKit.shutdownActorSystem(system);
    }

    @Test
    public void testBrokerActor(){
        final TestKit testProbe = new TestKit(system);
        final ActorRef brokerActor = system.actorOf(Props.create(Player.class), "Player");
        brokerActor.tell(new Player.Getdata("La"), ActorRef.noSender());
        Player.Getdata thisClass  = testProbe.expectMsgClass(Player.Getdata.class);
        Assert.assertEquals(Player.Getdata.class , thisClass.getClass());
    }

}
